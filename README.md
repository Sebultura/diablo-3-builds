# Diablo 3 Builds
# [EN]
This is a public repository for the builds compilation I have made for different Diablo 3 patches and seasons.
Permission is granted to download and share those files, but a small credit note for my work is always appreciated ;)

\* To download the whole library of PDF files: click on the Downloads menu at the left, then on "Download repository"

\*To download individual PDF files: click on the file name, then on "View raw"

# [FR]
Ceci est un dépôt publique contenant les compilations de builds que j'ai préparé pour les différent patchs Diablo 3 et les différentes saisons.
Vous avez ma permission de télécharger et partager ces fichiers, mais une petite note me référençant pour ce travail est toujours appréciée ;)

\* Pour télécharger la bibliothèque complète de fichiers PDF: cliquez sur le menu Downloads à gauche, puis sur "Download repository"

\* Pour télécharger des fichiers PDF individuels: cliquez sur le nom du fichier, puis sur "View raw"